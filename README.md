Migrate Process Newspaper3k
================
This module provides a Migrate process plugin to enable you to request and 
extract data from the python based Newspaper3k web scraper. The plugin
returns this json object as an associative array.

Features
------------
1. Curate online articles using python based newspaper article curation library.
2. Specify current working directory (cwd) to enable you to run multiple
scripts. This feature also enables you to customise the process fro each
migration.

Prerequisite
------------
Web server capable of running a python3 script. 

See https://github.com/2dareis2do/newspaper3k-php-wrapper for installation and
setup instructions.

Installation
------------
Download the module using Composer with the command ```composer require
drupal/migrate_process_newspaper3k``` and enable it.

## JavaScript Redirects
if you are using feeds that utilise javascipt redirects consider using the
migrate_process_js_redirect_link module.

## Example Usage

```
process:
   'body/value':
     -
       plugin: migrate_process_js_redirect_link
       source: link
     -
       plugin: migrate_process_newspaper3k
       debug: false
       cwd: ../python/script/location
       command: /path/to/python // optional - default python3
     -
       plugin: skip_on_empty
       method: row
       message: 'migrate_process_newspaper3k import is empty'
     -
       plugin: extract
       index:
         - summary

```

## Schema Keys
See the included stub json for more info on this. Those of note include:
```
summary (string)
source_url (string)
url (string)
title (string)
top_img (string)
top_image (string)
meta_img (string)
imgs (array)
images (array)
movies (array)
text (string)
keywords (array)
meta_keywords (array)
tags (array)
authors (array)
publish_date (string)
summary (string)
html (string)
meta_data (array object)
meta_description (string)
article_html (string)
top_node (string)
doc (string)
```

For more info on how Newspaper3k parses articles please see:
https://newspaper.readthedocs.io/en/latest/user_guide/quickstart.html#parsing-an-article

-----------
* Daniel Lobo (2dareis2do)
