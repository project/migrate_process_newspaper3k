<?php

namespace Drupal\migrate_process_newspaper3k\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Twodareis2do\Scrape\Newspaper3kWrapper;

/**
 * Provides a 'MigrateProcessNewspaper3k' migrate process plugin.
 *
 * Example:
 *
 * @code
 *
 * process:
 *   'body/value':
 *     -
 *       plugin: migrate_process_js_redirect_link
 *       source: link
 *     -
 *       plugin: migrate_process_newspaper3k
 *       debug: true // default "false"
 *       command: /path/to/python // optional default "python"
 *       cwd: "./my/python/script" // accepts absolute or relative path
 *     -
 *       plugin: skip_on_empty
 *       method: row
 *       message: 'migrate_process_newspaper3k import failed'
 *     -
 *       plugin: extract
 *       index:
 *         - summary
 *
 * @endcode
 *
 * @see \Drupal\migrate\Plugin\MigrateProcessInterface
 *
 * @MigrateProcessPlugin(
 *  id = "migrate_process_newspaper3k"
 * )
 */
class MigrateProcessNewspaper3k extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The Newspaper3k wrapper client.
   *
   * @var \Twodareis2do\Scrape\Newspaper3kWrapper
   */
  protected $parser;

  /**
   * The Logger Class object.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a database object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $logger;

  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\migrate\MigrateSkipRowException
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    $http_string_position = strpos($value ?? '', 'http');

    // Disable Debugging by default.
    $debug = FALSE;
    $configuration = $this->configuration;

    // use global installed python3 interpreter by default
    $command = 'python3';

    // Check if we need to enable default support.
    if (isset($configuration["debug"]) && $configuration["debug"] === TRUE) {
      $debug = TRUE;
    }
    // This is useful as we can also set from the unit tests.
    if (isset($this->pluginDefinition) && isset($this->pluginDefinition["debug"]) && filter_var($this->pluginDefinition["debug"], FILTER_VALIDATE_BOOLEAN) === TRUE) {
      $debug = TRUE;
    }
    // Check if we need to pass a the current working directory (relative).
    if (isset($configuration["cwd"])) {
      $cwd = $configuration["cwd"];
    }
    // Check if we need to pass a the python command path (absolute).
    if (isset($configuration["command"])) {
      $command = $configuration["command"];
    }
    // Check if url is absolute (not relative) and is a url.
    if ($http_string_position === 0 && filter_var($value, FILTER_VALIDATE_URL) !== FALSE) {
      try {

        if (!isset($this->parser)) {
          $this->parser = new Newspaper3kWrapper();
        }
        // If debugging, Newspaper3kWrapper will output json to /tmp directory.
        if (isset($cwd)) {
          $output = $this->parser->scrape($value, $debug, $cwd, $command);
        }
        else {
          $output = $this->parser->scrape($value, $debug, null, $command);
        }

        if (!is_array($output)) {
          // Don't throw exception (skip row) but raise notice.
          $this->logger->notice('ProcessFailedException - Newspaper3k Expects an Array to be returned. The following was output: @output for @value', [
            '@output' => $output,
            '@value' => $value,
          ]);
          // Massage the value to be an acceptable (empty) value.
          return [];
        }

        return $output;

      }
      catch (MigrateException $e) {
        throw new MigrateException(sprintf('MigrateException - Newspaper3k Failed to get URL %s "%s". %s', $value, gettype($e), $e->getMessage()), $e->getCode(), $e);
      }
      catch (ProcessFailedException $e) {
        $message = sprintf('ProcessFailedException - Newspaper3k Failed to get URL %s "%s". %s', $value, gettype($e), $e->getMessage());
        throw new MigrateSkipRowException($message);
      }
      catch (\Exception $e) {

        // Logs a notice to channel if we get error response.
        // Avoid thowing an exception here as this would output as a string
        $this->logger->notice('Newspaper3k Failed to get (1) URL @url "@error". @code', [
          '@url' => $value,
          '@error' => $e->getMessage(),
          '@code' => $e->getCode(),
        ]);
        // If fails return empty array (we can handle this with skip_on_empty).
        return [];
      }
    }
    else {
      // If string does not start with http return empty array.
      return [];
    }

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.channel.migrate_process_newspaper3k')
    );
  }

}
