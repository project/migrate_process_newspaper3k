<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_process_newspaper3k\Unit\process;

use Drupal\migrate_process_newspaper3k\Plugin\migrate\process\MigrateProcessNewspaper3k;
use Drupal\Tests\migrate\Unit\process\MigrateProcessTestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Psr\Log\LoggerInterface;

/**
 * Tests the migrate_process_newspaper3k process plugin.
 *
 * @group migrate_process_newspaper3k
 * @coversDefaultClass \Drupal\migrate_process_newspaper3k\Plugin\migrate\process\MigrateProcessNewspaper3kTest
 */
final class MigrateProcessNewspaper3kTest extends MigrateProcessTestCase {

  /**
   * A logger prophecy object.
   *
   * Using ::setTestLogger(), this prophecy will be configured and injected into
   * the container. Using $this->logger->function(args)->shouldHaveBeenCalled()
   * you can assert that the logger was called.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected $logger;

  /**
   * Mock of http_client service.
   *
   * @var \GuzzleHttp\Client
   */
  protected $mockHttp;

  /**
   * The data fetcher plugin definition.
   */
  private array $pluginDefinition = [
    'id' => 'migrate_process_newspaper3k',
    'title' => 'Migrate Process Newspaper3k',
    'jsredirect' => TRUE,
  ];

  /**
   * Test MigrateProcessnewspaper3k returns json.
   */
  public function testMigrateProcessNewspaper3k() {
    $configuration = [];
    $value = 'https://news.google.com/rss/articles/CBMiVWh0dHBzOi8vd3d3LmxvbmRvbi1maXJlLmdvdi51ay9pbmNpZGVudHMvMjAyMy9qYW51YXJ5L21haXNvbmV0dGUtZmlyZS1zdHJlYXRoYW0taGlsbC_SAQA?oc=5';

    $html = file_get_contents(__DIR__ . '/../../../fixtures/files/uk-england-london-68406862.html');
    $json = file_get_contents(__DIR__ . '/../../../fixtures/files/testwww.bbc.co.uk.json');
    $associative_array = json_decode($json, TRUE);
    $mock = new MockHandler([
      new Response(200, [], $html),
    ]);
    $handler = HandlerStack::create($mock);
    $this->mockHttp = new Client(['handler' => $handler]);

    $this->logger = $this->prophesize(LoggerInterface::class);

    $document = (new MigrateProcessNewspaper3k($configuration, 'migrate_process_newspaper3k', $this->pluginDefinition, $this->mockHttp, $this->logger->reveal()))
      ->transform($value, $this->migrateExecutable, $this->row, 'destinationproperty');

    $this->assertEquals($associative_array, $document, $message = "actual value is not equal to expected");
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    $this->row = $this->getMockBuilder('Drupal\migrate\Row')
      ->disableOriginalConstructor()
      ->getMock();

    $this->migrateExecutable = $this->getMockBuilder('Drupal\migrate\MigrateExecutable')
      ->disableOriginalConstructor()
      ->getMock();

    $mockHttp = $this
      ->getMockBuilder('GuzzleHttp\Client')
      ->disableOriginalConstructor()
      ->addMethods(['getBody'])
      ->onlyMethods(['get'])
      ->getMock();
    $this->mockHttp = $mockHttp;

    parent::setUp();

  }

}
